((root, factory) ->
  if !MINIFY and ( typeof define is "function" and define.amd )
    ###* Set up CTSChart appropriately for the environment. Start with AMD. ###
    define ['../../jnc'], (CTS) ->
      factory(CTS)

  else if !MINIFY and typeof exports isnt "undefined"
    ###* Next for Node.js or CommonJS. jQuery may not be needed as a module. ###
    CTS = require('../../jnc')
    factory(CTS)

  else
    ###* Finally, as a browser global. ###
    factory(root.CTS)
) this, (CTS) ->

  CTS.QuickInfo =
    Views: {}
    Models: {}
    Interactions: {}
    Service: {}
    CONFIG:
     URL:  '//backend-dev.CTS.io'
     FEATURE:
       DISTANCE_ENABLED: true
       MAGAZINES_ENABLED: true
       WEATHER_ENABLED: true
       MARKER_ENABLED: true
       EDIT_MAP_ENABLED: true
       ADD_WAYPOINT_ENABLED: true
       DRIVE_TO_ENABLED: true

    Enums:
      "Categories":
        "Generic_Info" : 0
        "Composition_of_Seabed": 0.5
        "Rock_Wreck_Obstruction": 1
        "Buoys": 2
        "Beacons_and_Signs": 3
        "Lights": 4
        "Shop_and_Repairs": 5
        "Ports_and_Marinas": 6
        "Anchorages_Mooring": 7

      "Generic_Info":
        "TextNote" : 0                  #TNDX_NAME_INFORM
        "Fishing_Spot": 1               #TNDX_NAME_INFORM_VALSOU_DUNITS
        "Diving_Spot": 2                #TNDX_NAME_INFORM_VALSOU_DUNITS
        "Sounding": 3                   #TNDX_VALSOU_DUNITS
        "Water_Turbolence": 4           #TNDX_NAME_INFORM
        "Trail_Head": 5                 #TNDX_NAME_INFORM
        "Water_Access": 6               #TNDX_NAME_INFORM

      "Composition_of_Seabed":
        "Mud": 5                        #TNDX_INFORM
        "Clay": 6                       #TNDX_INFORM
        "Silt": 7                       #TNDX_INFORM
        "Sand": 8                       #TNDX_INFORM
        "Stone": 9                      #TNDX_INFORM
        "Gravel": 10                    #TNDX_INFORM
        "Pebbles": 11                   #TNDX_INFORM
        "Cobbles": 12                   #TNDX_INFORM
        "Rock": 13                      #TNDX_INFORM
        "Lava": 14                      #TNDX_INFORM
        "Coral": 15                     #TNDX_INFORM
        "Shells": 16                    #TNDX_INFORM
        "Boulder": 17                   #TNDX_INFORM

      "Rock_Wreck_Obstruction":
        "Rock_always_dry": 0            #TNDX_NAME_INFORM_VALSOU_DUNITS
        "Rock_cover_uncover": 1         #TNDX_NAME_INFORM_VALSOU_DUNITS
        "Rock_under_water": 2           #TNDX_NAME_INFORM_VALSOU_DUNITS
        "Bottom_obstruction": 3         #TNDX_NAME_INFORM_VALSOU_DUNITS
        "Wreck_non_dangerous": 4        #TNDX_NAME_INFORM_VALSOU_DUNITS
        "Wreck_dangerous": 5            #TNDX_NAME_INFORM_VALSOU_DUNITS
        "Wreck_emerging": 6             #TNDX_NAME_INFORM_VALSOU_DUNITS
        "Offshore_platform": 7          #TNDX_NAME_INFORM
        "Fishing_facility": 8           #TNDX_NAME_INFORM
        "Pylon_bridge_support": 9       #TNDX_NAME_INFORM
        "Lock_Gate": 10                 #TNDX_NAME_INFORM
        "Dam": 11                       #TNDX_NAME_INFORM

      "Buoys":
        "Green_lateral": 0              #TNDX_NAME_INFORM_FOGSIG_LIGHT
        "Red_lateral": 1                #TNDX_NAME_INFORM_FOGSIG_LIGHT
        "Green_Red": 2                  #TNDX_NAME_INFORM_FOGSIG_LIGHT
        "Red_Green": 3                  #TNDX_NAME_INFORM_FOGSIG_LIGHT
        "Green_Red_Green": 4            #TNDX_NAME_INFORM_FOGSIG_LIGHT
        "Red_Green_Red": 5              #TNDX_NAME_INFORM_FOGSIG_LIGHT
        "Keep_North": 6                 #TNDX_NAME_INFORM_FOGSIG_LIGHT
        "Keep_East": 7                  #TNDX_NAME_INFORM_FOGSIG_LIGHT
        "Keep_South": 8                 #TNDX_NAME_INFORM_FOGSIG_LIGHT
        "Keep_West": 9                  #TNDX_NAME_INFORM_FOGSIG_LIGHT
        "Isolated_Danger": 10           #TNDX_NAME_INFORM_FOGSIG_LIGHT
        "Safe_Water": 11                #TNDX_NAME_INFORM_FOGSIG_LIGHT
        "Special_Purpose": 12           #TNDX_NAME_INFORM_FOGSIG_LIGHT
        "Mooring_buoy":  13             #TNDX_NAME_INFORM

      "Beacons_and_Signs":
        "Green_lateral": 0              #TNDX_NAME_INFORM_FOGSIG_LIGHT
        "Red_lateral": 1                #TNDX_NAME_INFORM_FOGSIG_LIGHT
        "Green_Red": 2                  #TNDX_NAME_INFORM_FOGSIG_LIGHT
        "Red_Green": 3                  #TNDX_NAME_INFORM_FOGSIG_LIGHT
        "Green_Red_Green": 4            #TNDX_NAME_INFORM_FOGSIG_LIGHT
        "Red_Green_Red": 5              #TNDX_NAME_INFORM_FOGSIG_LIGHT
        "Keep_North": 6                 #TNDX_NAME_INFORM_FOGSIG_LIGHT
        "Keep_East": 7                  #TNDX_NAME_INFORM_FOGSIG_LIGHT
        "Keep_South": 8                 #TNDX_NAME_INFORM_FOGSIG_LIGHT
        "Keep_West": 9                  #TNDX_NAME_INFORM_FOGSIG_LIGHT
        "Isolated_Danger": 10           #TNDX_NAME_INFORM_FOGSIG_LIGHT
        "Safe_Water": 11                #TNDX_NAME_INFORM_FOGSIG_LIGHT
        "Special_Purpose": 12           #TNDX_NAME_INFORM_FOGSIG_LIGHT
        "Distance_mark": 13             #TNDX_NAME_INFORM

      "Lights":
        "Red": 0                        #TNDX_NAME_INFORM
        "Green": 1                      #TNDX_NAME_INFORM
        "Yellow": 2                     #TNDX_NAME_INFORM
        "White": 3                      #TNDX_NAME_INFORM
        "Any_other_color": 4            #TNDX_NAME_INFORM
        "Light_float": 5                #TNDX_NAME_INFORM
        "Light_vessel": 6               #TNDX_NAME_INFORM

      "Shop_and_Repairs":
        "Restaurant": 0                  #TNDX_PSGIIO_1
        "Bar": 1                         #TNDX_PSGIIO_1
        "Marine_shop_Chandler": 2        #TNDX_PSGIIO_1
        "Groceries_Provisioning": 3      #TNDX_PSGIIO_1
        "Fishing_equipment": 4           #TNDX_PSGIIO_1
        "Diving_Shop": 5                 #TNDX_PSGIIO_1
        "Boat_Kayak_charter": 6          #TNDX_PSGIIO_1
        "Boat_Dealer_Repair": 7          #TNDX_PSGIIO_1
        "Engine_dealer": 8               #TNDX_PSGIIO_1
        "Electronic_dealer": 9           #TNDX_PSGIIO_1
        "Boatyard": 10                   #TNDX_PSGIIO_1
        "Other_repairs": 11              #TNDX_PSGIIO_1
        "Yacht_Broker": 12               #TNDX_PSGIIO_1
        "Marine_Apparel": 13             #TNDX_PSGIIO_1
        "Pharmacy": 14                   #TNDX_PSGIIO_1

      "Ports_and_Marinas":
        "Port_Harbor": 0                  #TNDX_MARINA
        "Marina": 1                       #TNDX_MARINA
        "Fuel_Dock": 2                    #TNDX_PSGIIO_PRCLST
        "Dinghy_Dock": 3                  #TNDX_NAME_INFORM
        "Public_Dock": 4                  #TNDX_PSGIIO_1
        "Water_taxi": 5                   #TNDX_PSGIIO_VHF
        "Yacht_Club": 6                   #TNDX_PSGIIO_VHF
        "Harbor_Master": 7                #TNDX_PSGIIO_VHF
        "Coastguard_station": 8           #TNDX_NAME_INFORM
        "Travel_Lift": 9                  #TNDX_NAME_INFORM
        "Boat_ramp": 10                   #TNDX_NAME_INFORM
        "Towing_service": 11              #TNDX_PSGIIO_VHF
        "Maritime_Museum": 12             #TNDX_PSGIIO_1
        "Ferry_Dock": 13                  #TNDX_NAME_INFORM
        "Sailing_School": 14              #TNDX_PSGIIO_1

      "Anchorages_Mooring":
        "Anchorage": 0                    #TNDX_NAME_INFORM
        "Mooring_Service": 1              #TNDX_PSGIIO_VHF
