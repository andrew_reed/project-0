((root, factory) ->
  if !MINIFY and ( typeof define is "function" and define.amd )
    ###* Set up CTSChart appropriately for the environment. Start with AMD. ###
    define ['../../../jnc', '../../../views/TranslatableView', '../../../utils/Converter', '../../../utils/GeneralUtils', '../QuickInfo'], (CTS, TranslatableView, Converter, GeneralUtils) ->
      factory(CTS, CTS._, CTS.Backbone, TranslatableView, Converter, GeneralUtils)

  else if !MINIFY and typeof exports isnt "undefined"
    ###* Next for Node.js or CommonJS. jQuery may not be needed as a module. ###
    CTS = require('../../../jnc')
    TranslatableView = require('../../../views/TranslatableView')
    Converter = require('../../../utils/Converter')
    GeneralUtils = require('../../../utils/GeneralUtils')
    require('../QuickInfo')
    require('../../../config')
    factory(CTS, CTS._, CTS.Backbone, TranslatableView, Converter, GeneralUtils)

  else
    ###* Finally, as a browser global. ###
    factory(root.CTS, root.CTS._, root.CTS.Backbone, root.CTS.Views.TranslatableView, root.CTS.utils.Converter, root.CTS.utils.GeneralUtils)
) this, (CTS, _, Backbone, TranslatableView, Converter, GeneralUtils) ->

  class CTS.QuickInfo.Models.ObjectInfo extends Backbone.Model
    #urlTemplate: null
    repository: null

    initialize: (arr = [], options = {}) ->
      _.defaults options

      @repository = options.repository

    fetch: (id, options = {}) ->
      @trigger 'request'
      @clear({silent: true})
      @id = id

      @repository?.getDetails @id, (model) =>
        @set model
        options?.success @
        @trigger 'sync'
