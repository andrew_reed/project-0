((root, factory) ->
  if !MINIFY and ( typeof define is "function" and define.amd )
    ###* Set up CTSChart appropriately for the environment. Start with AMD. ###
    define ['../../../jnc', '../QuickInfo'], (CTS) ->
      factory(CTS)

  else if !MINIFY and typeof exports isnt "undefined"
    ###* Next for Node.js or CommonJS. jQuery may not be needed as a module. ###
    CTS = require('../../jnc')
    require '../QuickInfo'
    factory(CTS)

  else
    ###* Finally, as a browser global. ###
    factory(root.CTS)
) this, (CTS) ->

  class CTS.QuickInfo.Service.RepositoryInterface

    getQuickInfoList: (coordinate, callback) ->
      console.log 'CTS.QuickInfo.Service.Repository#getQuickInfoList is not implemented'

    getDetails: (id, callback) ->
      console.log 'CTS.QuickInfo.Service.Repository#getDetails is not implemented'

    parse: (response, options) ->
      if response.items?
        super response.items, options
