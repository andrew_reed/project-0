((root, factory) ->
  if !MINIFY and ( typeof define is "function" and define.amd )
    ###* Set up CTSChart appropriately for the environment. Start with AMD. ###
    define ['../../../jnc', './RepositoryInterface'], (CTS, RepositoryInterface) ->
      factory(CTS, RepositoryInterface)

  else if !MINIFY and typeof exports isnt "undefined"
    ###* Next for Node.js or CommonJS. jQuery may not be needed as a module. ###
    CTS = require('../../jnc')
    RepositoryInterface = require('../../RepositoryInterface')
    factory(CTS, RepositoryInterface)

  else
    ###* Finally, as a browser global. ###
    factory(root.CTS, root.CTS.QuickInfo.Service.RepositoryInterface)
) this, (CTS, RepositoryInterface) ->

  class CTS.QuickInfo.Service.DummyRepository extends RepositoryInterface


    _findItemDetails: (id) ->
      response ={
        'details': true
        'position':
          'lat': 43.716095741793936
          'lon': 10.382508115308703
        'id': 'Y2F0ZWdvcnk9MjE0JmNvZGU9MTg4JmZpbGVuYW1lPTExXzEwODNfNzQ2JnJlY19obmRsPTI3JnJlY19zdGF0PTE='
        'name': 'Agip'
        'action':
          'editable': false
          'routable': false
          "review": {
            "rating": 4,
            "review_num": 10,
            "rating_num": 10
          }
        'properties': [
            {
                'fuel': [
                    {
                        'label': 'Gasoline'
                        'type': 'GAS'
                        'val': 1.9229999780654907
                        'date': 1346112000
                    },
                    {
                        'label': 'Diesel'
                        'type': 'DIESEL'
                        'val': 1.8049999475479126
                        'date': 1346112000
                    }
                ]
            },
          {
            'label': 'AGIP  - Pisa .\n'
          },
          {
            'label': 'Icona Label'
            'icon': 'icona'
          },
          {
            'label': ', Pisa'
            'icon': 'icona'
            'link': 'https://www.google.it/maps/dir//43° 42.966\' N,10° 22.950\' E/'
          },
          { 'label': 'Last modified by: Cig\nOn: Tue, 28 Aug 2012 08:18:09 GMT' },
          {
            'title': 'Title-1'
            'fields': [
              {
                'label': 'Title-1A'
              },
              {
                'label': 'Title-1B'
              }
            ]
          },
            {
              'title': ''
              'fields': [
                {
                  'label': 'empty-C'
                },
                {
                  'label': 'empty-D'
                }
              ]
            },
            {
              'fields': [
                {
                  'label': 'undefinied-E'
                },
                {
                  'label': 'undefinied-F'
                }
              ]
            }
        ]
      }

      if id == 1
        response.action.editable=true
      else if id == 2
        response.action.routable=true

      response


    getQuickInfoList: (coordinate, callback) ->
      response ={
       "items": [
        {
          "id": "100",
          "__category": "8",
          "name": "Simple",
          "icon_id": "current",
          "details": false,
          "distance": "10 NM",
          "position": {
            "lon": 21.55,
            "lat": 49.1
          },
          "current": {
            "icon_type": "current_up",
            "value": 1.2,
            "angle": 30
          }
        },
        {
          "id": "1",
          "__category": "6",
          "name": "yes edit + no route",
          "icon_id": "fuel_01",
          "details": true,
          "distance": "10 NM",
          "review": {
            "rating": 4,
            "review_num": 10,
            "rating_num": 10
          },
          "position": {
            "lon": 21.55,
            "lat": 49.1
          }
        },
        {
          "id": "2",
          "name": "no edit + yes route",
          "icon_id": "marine_01",
          "details": true,
          "distance": "10 NM",
          "phone": {
            "label": "+39 0565914121",
            "icon": "",
            "link": "tel:+39 0565914121"
          },
          "position": {
            "lon": 21.55,
            "lat": 49.1
          },
          "review": {
            "rating": 4,
            "review_num": 10,
            "rating_num": 10
          }
        },

        {
          "id": "3",
          "__category": "1",
          "name": "Pescara",
          "icon_id": "marine_01",
          "details": true,
          "distance": "10 NM",
          "phone": {
            "label": "+39 0565914121",
            "icon": "",
            "link": "tel:+39 0565914121"
          },
          "position": {
            "lat": 10,
            "lon": 10
          },
          "review": {
            "rating": 4,
            "review_num": 10,
            "rating_num": 10
          },
          "vhf": {
            "vhf": "12.3",
            "vhf_m": "13.3"
          }
        },
        {
          "id": "4",
          "__category": "1",
          "name": "Pescara no vhf_m",
          "icon_id": "marine_01",
          "details": true,
          "distance": "10 NM",
          "phone": {
            "label": "+39 0565914121",
            "icon": "",
            "link": "tel:+39 0565914121"
          },
          "position": {
            "lat": 10,
            "lng": 10
          },
          "review": {
            "rating": 4,
            "review_num": 10,
            "rating_num": 10
          },
          "vhf": {
            "vhf": "12.3"
          }
        },
        {
          "id": "5",
          "__category": "1",
          "name": "Pescara no vhf master",
          "icon_id": "marine_01",
          "details": true,
          "distance": "10 NM",
          "phone": {
            "label": "+39 0565914121",
            "icon": "",
            "link": "tel:+39 0565914121"
          },
          "position": {
            "lat": 10,
            "lng": 10
          },
          "review": {
            "rating": 4,
            "review_num": 10,
            "rating_num": 10
          },
          "vhf": {
            "vhf_m": "13.3"
          }
        },
        {
          "id": "6",
          "__category": "1",
          "name": "Fuel",
          "icon_id": "marine_01",
          "details": true,
          "distance": "10 NM",
          "phone": {
           "label": "+39 0565914121",
           "icon": "",
           "link": "tel:+39 0565914121"
          },
          "position": {
           "lon": 21.55,
           "lat": 49.1
          },
          "fuel": [
            {
             "type": "GAS",
             "label": "benzina",
             "val": 1.23,
             "date": 1234567897
            },
            {
             "type": "DIESEL",
             "label": "gasoline",
             "val": 1.43,
             "date": 1234567897
            }
          ]
        },
        {
          "id": "90",
          "__category": "7",
          "name": "Fuel 2",
          "icon_id": "fuel_01",
          "details": true,
          "distance": "10 NM",
          "review": {
           "rating": 4,
           "review_num": 10,
           "rating_num": 10
          },
          "position": {
           "lon": 24.12,
           "lat": 40.1
          },
          "fuel": [
            {
             "type": "GAS",
             "label": "benzina",
             "val": 1.23,
             "date": 1234567897
            },
            {
             "type": "DIESEL",
             "label": "gasoline",
             "val": "",
             "date": ""
            }
          ]
        },
        {
          "id": "100",
          "__category": "7",
          "name": "API",
          "icon_id": "fuel_01",
          "details": true,
          "distance": "10 NM",
          "review": {
           "rating": 4,
           "review_num": 10,
           "rating_num": 10
          },
          "position": {
           "lon": 23,
           "lat": 40.1
          },
          "fuel": [
            {
             "type": "GAS",
             "label": "benzina",
             "val": 1.23,
             "date": 1234567897
            },
            {
             "type": "DIESEL",
             "label": "gasoline",
             "val": 1.43,
             "date": 1234567897
            }
          ]
        },
        {
          "id": "110",
          "__category": "9",
          "name": "1422_Dover_V",
          "icon_id": "current",
          "details": true,
          "distance": "10 NM",
          "position": {
           "lon": 21.55,
           "lat": 49.1
          },
          "current": {
           "icon_type": "current_up",
           "value": 1.2,
           "angle": 30
          }
        },
        {
          "id": "120",
          "__category": "9",
          "name": "1422_Dover_V",
          "icon_id": "current",
          "details": true,
          "distance": "10 NM",
          "position": {
           "lon": 21.55,
           "lat": 49.1
          },
          "current": {
           "icon_type": "current_up",
           "value": 1.2,
           "angle": 30
          }
        },
        {
         "id": "10",
         "__category": "8",
         "name": "La Spezia",
         "icon_id": "tide",
         "details": true,
         "position": {
          "lat": 10,
          "lon": 10
         },
         "tide": {
          "distance": "10 NM",
          "icon_type": "tide_up",
          "value": 5.3
         }
        },
       ]
      };
      setTimeout () =>
        callback response
      , 1000

    getDetails: (id, callback) ->
      setTimeout () =>
        callback @_findItemDetails(id)
      , 1000
