((root, factory) ->
  if !MINIFY and ( typeof define is "function" and define.amd )
    ###* Set up CTSChart appropriately for the environment. Start with AMD. ###
    define ['../../../jnc', '../../../widgets/Popup', './QuickInfoToolBar', '../QuickInfo'], (CTS, Popup, QuickInfoToolBar) ->
      factory(CTS, CTS._, CTS.$, Popup, QuickInfoToolBar)

  else if !MINIFY and typeof exports isnt "undefined"
    ###* Next for Node.js or CommonJS. jQuery may not be needed as a module. ###
    CTS = require('../jnc')
    Popup = require('../../../widgets/Popup')
    QuickInfoToolBar = require('./QuickInfoToolBar')
    require('../QuickInfo')
    require( '../../../config')
    factory(CTS, CTS._, CTS.$, Popup, QuickInfoToolBar)
  else
    ###* Finally, as a browser global. ###
    factory(root.CTS, root.CTS._, root.CTS.$, root.CTS.Widgets.Popup, root.CTS.QuickInfo.Views.QuickInfoToolBar)
) this, (CTS, _, $, BasePopup, QuickInfoToolBar) ->

  ###*
    Popup window widget
    @private
    @class
    @name CTS.Widgets.ModalPopup
  ###
  class CTS.QuickInfo.Views.QuickInfoPopup extends BasePopup
    template: CTS.TPL['quickInfo/quickInfoPopup']
    itemTemplate: CTS.TPL['quickInfo/POI/quick_info']

    $toolBar = null

    ###*
      UI event bindings
      @memberof CTS.Widgets.ModalPopup
    ###
    events: ->
      _.extend super,
        'click .go-to-map': 'onMap'
        'click [data-item-id]': "_onPOI"


    ui: _.extend {}, QuickInfoPopup.__super__.ui,
      preloaderContainerSelector: '[data-region="list"]'

    regions:
      toolBar: '[data-region="toolbar"]'
      list: '[data-region="list"]'

    ###*
      Init ModalPopup
      @memberof CTS.Widgets.ModalPopup
    ###
    initialize: (options = {}) ->
      @$toolBar = new QuickInfoToolBar
        enableObjectInfoFeature: false
        enabledUGC: options.enabledUGC
      @$toolBar.render()

      super _.extend options,
        html: ''

      @assetsUrl = options.assets

    enabledUGC: (value)->
      @$toolBar.enableUGC(value)
      @$toolBar.render()

    show: (coordinate = [0, 0]) ->
      #console.log("show a")
      @coordinate=coordinate
      @$popup = $ @_renderTemplate(coordinate)
      super
      @$toolBar.render()
      @$popup.find(@regions.toolBar).append(@$toolBar.$el)
      $container = @$(@regions.list)

      @collection?.fetch coordinate,
        success: =>
          @collection.each (object) =>
            ctx = _.extend object.toJSON(),
              translate: @translate
              assetsUrl: @assetsUrl
            $container.append(@itemTemplate(ctx))

    hide: ->
      @$toolBar?.$el.detach()
      super

    _renderTemplate: (coordinate) ->
      @coordinate=coordinate
      context = _.extend {},
        translate: @translate
        formatLatitude: @formatLatitude
        formatLongitude: @formatLongitude
        coordinate: coordinate
        assetsUrl: @assetsUrl

      @template(context)

    onMap: (e) ->
      e.preventDefault()
      @hide()

    _onPOI: (e) ->
      dataMe = $(e.currentTarget).data()
      if dataMe.hasDetails?
        id = dataMe.itemId
        @trigger 'show:info', id


