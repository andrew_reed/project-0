((root, factory) ->
  if !MINIFY and ( typeof define is "function" and define.amd )
    ###* Set up CTSChart appropriately for the environment. Start with AMD. ###
    define ['../../../jnc', '../../../views/TranslatableView', '../QuickInfo'], (CTS, TranslatableView) ->
      factory(CTS, CTS._, TranslatableView)

  else if !MINIFY and typeof exports isnt "undefined"
    ###* Next for Node.js or CommonJS. jQuery may not be needed as a module. ###
    CTS = require('../jnc')
    TranslatableView = require('../../../views/TranslatableView')
    require('../QuickInfo')

    factory(CTS, CTS._, TranslatableView)
  else
    ###* Finally, as a browser global. ###
    factory(root.CTS, root.CTS._, root.CTS.Views.TranslatableView)
) this, (CTS, _, TranslatableView) ->

  class CTS.QuickInfo.Views.QuickInfoToolBar extends TranslatableView
    template: CTS.TPL['quickInfo/quickInfoToolBar']

    initialize: (options = { enableObjectInfoFeature: false, enabledUGC: false}) ->
      @isObjectInfo = options.enableObjectInfoFeature
      @enabledUGC = options.enabledUGC

    enableUGC: (value)->
      @enabledUGC=value

    setMyModel: (model)->
      #console.log(model)
      @myModel=model

    getFeatures: ->
      distance:
        title: 'quickInfo.distance'
        enabled: CTS.QuickInfo.CONFIG.FEATURE.DISTANCE_ENABLED
      magazines:
        title: 'quickInfo.magazines'
        enabled: CTS.QuickInfo.CONFIG.FEATURE.MAGAZINES_ENABLED
      weather:
        title: 'quickInfo.weather'
        enabled: CTS.QuickInfo.CONFIG.FEATURE.WEATHER_ENABLED
      marker:
        title: 'quickInfo.marker'
        enabled: CTS.QuickInfo.CONFIG.FEATURE.MARKER_ENABLED

    getActions: ->
      actions = {}

      if @isObjectInfo == false
        if @enabledUGC
          actions.edit_map=
            class: "edit_map"
            title: 'buttons.edit_map'
            color: 'green'
            enabled: CTS.QuickInfo.CONFIG.FEATURE.EDIT_MAP_ENABLED
            icon: false

      if @myModel?.action.routable
        actions.drive_to=
          class: 'drive-to'
          title: 'buttons.drive_to'
          color: 'green'
          enabled: CTS.QuickInfo.CONFIG.FEATURE.DRIVE_TO_ENABLED
          icon: 'na-icon-drive-to'
          href: 'http://www.google.com/maps/dir//'  + @myModel.position.lat +  ',' + @myModel.position.lon

      actions.add_waypoint=
            class: "add_waypoint"
            title: 'buttons.boat_to'
            color: 'light-blue'
            enabled: CTS.QuickInfo.CONFIG.FEATURE.ADD_WAYPOINT_ENABLED
            icon: 'na-icon-boat-to'

      #console.log actions

      actions


    render:  ->
      context = _.extend {},
        translate: @translate
        features: @_filter @getFeatures()
        actions:  @_filter @getActions()

      @$el.html(@template(context))

    _filter: (obj) ->
      _.chain(obj)
        .map (item, key) ->
            item.key = key
            item
        .where({ enabled: true })
        .value()
