((root, factory) ->
  if !MINIFY and ( typeof define is "function" and define.amd )
    ###* Set up CTSChart appropriately for the environment. Start with AMD. ###
    define ['../../../jnc', '../QuickInfo'], (CTS) ->
      factory(CTS, CTS._, CTS.Backbone)

  else if !MINIFY and typeof exports isnt "undefined"
    ###* Next for Node.js or CommonJS. jQuery may not be needed as a module. ###
    CTS = require('../../../jnc')
    QuickInfo = require '../QuickInfo'
    factory(CTS, CTS._, CTS.Backbone)

  else
    ###* Finally, as a browser global. ###
    factory(root.CTS, root.CTS._, root.CTS.Backbone)
) this, (CTS, _, Backbone) ->
  
  class CTS.QuickInfo.Interactions.MapPointInteractionInterface

    EVENTS:
      MAP_CLICK: 'click'
      MAP_MOVE_START: 'start'
      MAP_MOVE_END: 'end'

    destroy: ->
      console.log 'CTS.QuickInfo.Interactions.MapPointInteractionInterface'

    getContainer: ->
      console.log 'CTS.QuickInfo.Interactions.getContainer'

    getCoordinate: ->
      console.log 'CTS.QuickInfo.Interactions.getCoordinate'

    getPixelPosition: ->
      console.log 'CTS.QuickInfo.Interactions.getPixelPosition'

    setCoordinate: (standardMercatorCoordinate) ->
      console.log 'CTS.QuickInfo.Interactions.setCoordinate'

    setPixelPosition: (pixelPosition) ->
      console.log 'CTS.QuickInfo.Interactions.setPixelPosition'

    startListeningEvents: ->
      console.log 'CTS.QuickInfo.Interactions.startListeningEvents'

    stopListeningEvents: ->
      console.log 'CTS.QuickInfo.Interactions.stopListeningEvents'

    _.extend(MapPointInteractionInterface.prototype, Backbone.Events)