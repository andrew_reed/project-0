((root, factory) ->
  if !MINIFY and ( typeof define is "function" and define.amd )
    ###* Set up CTSChart appropriately for the environment. Start with AMD. ###
    define ['../../../jnc', './MapPointInteractionInterface', '../../../utils/Converter', '../../../utils/GeneralUtils', '../QuickInfo'], (CTS, MapPointInteractionInterface, Converter, GeneralUtils) ->
      factory(CTS, CTS.$, CTS._, MapPointInteractionInterface, Converter, GeneralUtils)

  else if !MINIFY and typeof exports isnt "undefined"
    ###* Next for Node.js or CommonJS. jQuery may not be needed as a module. ###
    CTS = require('../../../jnc')
    MapPointInteractionInterface = require('./MapPointInteractionInterface')
    Converter = require('../../../utils/Converter')
    GeneralUtils = require('../../../utils/GeneralUtils')
    require('../QuickInfo')
    factory(CTS, CTS.$, CTS._, MapPointInteractionInterface, Converter, GeneralUtils)

  else
    ###* Finally, as a browser global. ###
    factory(root.CTS, root.CTS.$, root.CTS._, root.CTS.QuickInfo.Interactions.MapPointInteractionInterface, root.CTS.utils.Converter, root.CTS.utils.GeneralUtils)
) this, (CTS, $, _, MapPointInteractionInterface, Converter, GeneralUtils) ->

  class CTS.QuickInfo.Interactions.OLMapPointInteraction extends MapPointInteractionInterface
    map: null
    mapView: null
    coordinate: [0, 0] #web mercator coordinate
    pixelPosition: [0, 0]

    constructor: (map) ->
      @mapView = map
      @map = @mapView.getRowMap()

    destroy: ->
      @stopListeningEvents()

    getContainer: ->
      @mapView._olMapDiv

    getCoordinate: ->
      Converter.webMercatorToStandard(@coordinate)

    getPixelPosition: ->
      @pixelPosition

    setCoordinate: (standardMercatorCoordinate) ->
      @coordinate = Converter.standardMercatorToWeb(standardMercatorCoordinate)
      @pixelPosition = @map.getPixelFromCoordinate(@coordinate)

    setPixelPosition: (pixelPosition) ->
      @pixelPosition = pixelPosition
      @coordinate = @map.getCoordinateFromPixel @pixelPosition

    startListeningEvents: ->
      return if @_singleClickEventKey or @_dragEventKey or @_moveEventKey

      @_singleClickEventKey = @map.on 'singleclick',(e) =>
        pixelPosition = if GeneralUtils.isFirefox() then GeneralUtils.getPointerCoordinateForCanvas(e.originalEvent.originalTarget, e.originalEvent) else e.pixel
        @setPixelPosition(pixelPosition)
        @trigger @EVENTS.MAP_CLICK, @getCoordinate(), @getPixelPosition()

      @_dragEventKey = @map.on 'pointerdrag',(e) =>
        @trigger @EVENTS.MAP_MOVE_START

      @_moveEventKey = @map.on 'moveend',(e) =>
        #Update coordinate according to moved map view
        @setPixelPosition(@pixelPosition)
        @trigger  @EVENTS.MAP_MOVE_END, @getCoordinate(), @getPixelPosition()

    stopListeningEvents: ->
      @map.unByKey @_singleClickEventKey
      @map.unByKey @_dragEventKey
      @map.unByKey @_moveEventKey
      @_singleClickEventKey = @_dragEventKey = @_moveEventKey = null