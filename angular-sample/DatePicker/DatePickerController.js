define(["require", "exports", "moment", "App/Names", "../Common/DateTime/DateRange", "./DateRangeFormatter"],
    function (require, exports, moment, Names, DateRange, DateRangeFormatter) {

    var DatePickerController = (function () {

        function DatePickerController($scope) {
            var _this = this;
            this.$scope = $scope;
            this.dateRangeFormatter = new DateRangeFormatter();
            $scope.$watch(function () { return $scope.selection; }, function () { return _this.selectionChanged(); });
            $scope.$watch(function () { return $scope.minDate; }, function () { return _this.minDateChanged(); });
            $scope.$watch(function () { return $scope.maxDate; }, function () { return _this.maxDateChanged(); });
        }

        Object.defineProperty(DatePickerController.prototype, "calendarDate", {
            get: function () {
                return this._calendarDate;
            },
            set: function (date) {
                this._calendarDate = date;
                this.dateSelected(date);
            },
            enumerable: true,
            configurable: true
        });

        DatePickerController.prototype.dateDisabled = function (date) {
            return this.$scope.dateDisabled({ date: date });
        };

        DatePickerController.prototype.dateSelected = function (date) {
            var start = moment(date).startOf("day");
            start = this.toUtcKeepingTime(start);
            this.rangeSelected({ dateRange: new DateRange(start) });
        };

        DatePickerController.prototype.selectorSelected = function (selection) {
            this.rangeSelected(selection);
        };

        DatePickerController.prototype.rangeSelected = function (selection) {
            if (this.$scope.selection !== selection) {
                this.$scope.selection = selection;
                if (this.$scope.dateRangeSelected) {
                    this.$scope.dateRangeSelected({ selection: this.$scope.selection });
                }
            }
        };

        DatePickerController.prototype.selectionChanged = function () {
            var dateRange;
            if (!this.$scope.selection || !this.$scope.selection.dateRange) {
                dateRange = DateRange.currentDay;
            }
            else {
                dateRange = this.$scope.selection.dateRange;
            }
            this._calendarDate = dateRange.start.toDate();
        };

        DatePickerController.prototype.minDateChanged = function () {
            if (this.$scope.minDate) {
                var minDateMoment = this.toLocalKeepingTime(this.$scope.minDate);
                this.datePickerMinDate = minDateMoment.toDate();
            }
        };

        DatePickerController.prototype.maxDateChanged = function () {
            if (this.$scope.maxDate) {
                var maxDateMoment = this.toLocalKeepingTime(this.$scope.maxDate);
                maxDateMoment.add(-1, "seconds"); //we need this to be exclusive (implemented in Bootstrap as inclusive)
                this.datePickerMaxDate = maxDateMoment.toDate();
            }
        };

        DatePickerController.prototype.toLocalKeepingTime = function (value) {
            value = value.clone().local();
            value.add(value.zone(), "minutes");
            return value;
        };

        DatePickerController.prototype.toUtcKeepingTime = function (value) {
            value = value.clone();
            value = value.add(-value.zone(), "minutes");
            return value.utc();
        };

        DatePickerController.$inject = [Names.$scope];
        return DatePickerController;
    })();
    return DatePickerController;
});