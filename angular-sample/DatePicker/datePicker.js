/// <amd-dependency path="angular-ui-bootstrap" />
/// <amd-dependency path="text!./Views/popupDatePicker.html" />
/// <amd-dependency path="text!./Views/datePicker.html" />
/// <amd-dependency path="text!./Views/calendarDayPicker.html" />
/// <amd-dependency path="text!./Views/selectorsBar.html" />
/// <amd-dependency path="text!./Views/datePickerPopoutContent.html" />

define(["require", "exports", "angular", "App/Names", "./SelectorsBarController", "./PopupDatePickerController", "./DatePickerController", "./Selectors/DatePickerSelectors", "./DateRangeValidator", "./OpbPopupDatePickerController", "angular-ui-bootstrap", "text!./Views/popupDatePicker.html", "text!./Views/datePicker.html", "text!./Views/calendarDayPicker.html", "text!./Views/selectorsBar.html", "text!./Views/datePickerPopoutContent.html"],
    function (require, exports, angular, Names, SelectorsBarController, PopupDatePickerController, DatePickerController, DatePickerSelectors, DateRangeValidator, OpbPopupDatePickerController) {

    var datePicker = angular.module(Names.DatePicker.module, []);
    datePicker.service(Names.DatePicker.datePickerSelectors, DatePickerSelectors);
    datePicker.controller(Names.DatePicker.selectorsBarController, SelectorsBarController);
    datePicker.controller(Names.DatePicker.popupDatePickerController, PopupDatePickerController);
    datePicker.controller(Names.DatePicker.datePickerController, DatePickerController);
    datePicker.controller(Names.DatePicker.opbPopupDatePickerController, OpbPopupDatePickerController);

    var popupDatePickerDirective = {
        restrict: "E",
        replace: true,
        controller: Names.DatePicker.opbPopupDatePickerController,
        controllerAs: "controller",
        template: require("text!./Views/popupDatePicker.html"),
        scope: {
            isOpen: "=?",
            dateRangeSelected: "&",
            selectors: "=",
            minDate: "=?",
            maxDate: "=?",
            buttonClass: "@",
            iconButtonClass: "@",
            inputClass: "@",
            inputPlaceholder: "@",
            toggleMaxWidth: "=?",
            toggleWidth: "=?",
            selection: "=?",
            initialSelection: "=?",
            initialOpen: "=?",
            silentInitialise: "=?",
            toggle: "&",
            isDisabled: "=?",
            showIcon: "=?",
            rangeSelectionEnabled: "=?",
            dateDisabled: "&",
            persistentInput: "=?",
            popoutPosition: "@",
            popoutMyPosition: "@"
        }
    };

    datePicker.directive(Names.DatePicker.popupDatePickerDirective, function () {
        var directive = angular.extend({}, popupDatePickerDirective, {
            controller: Names.DatePicker.popupDatePickerController
        });
        return directive;
    });

    datePicker.directive(Names.DatePicker.opbPopupDatePickerDirective, function () {
        var directive = angular.extend({}, popupDatePickerDirective, {
            controller: Names.DatePicker.opbPopupDatePickerController
        });
        return directive;
    });

    datePicker.directive(Names.DatePicker.datePickerDirective, function () {
        var directive = {
            restrict: "E",
            replace: true,
            controllerAs: "controller",
            controller: Names.DatePicker.datePickerController,
            template: require("text!./Views/datePicker.html"),
            scope: {
                selection: "=?",
                dateRangeSelected: "&",
                selectors: "=",
                minDate: "=?",
                maxDate: "=?",
                dateDisabled: "&"
            }
        };
        return directive;
    });

    datePicker.directive(Names.DatePicker.datePickerPopoutDirective, [Names.Application.popoutCreator,
        function (popoutCreator) {
            return popoutCreator.create(Names.DatePicker.datePickerPopoutDirective);
        }
    ]);

    datePicker.directive(Names.DatePicker.datePickerPopoutContentDirective, function () {
        var directive = {
            restrict: "E",
            replace: true,
            template: require("text!./Views/datePickerPopoutContent.html"),
            scope: {
                popout: "="
            }
        };
        return directive;
    });

    datePicker.directive(Names.DatePicker.selectorsBarDirective, function () {
        var directive = {
            restrict: "E",
            replace: true,
            controller: Names.DatePicker.selectorsBarController,
            controllerAs: "controller",
            template: require("text!./Views/selectorsBar.html"),
            scope: {
                selectorSelected: "&",
                selectors: "="
            }
        };
        return directive;
    });

    datePicker.directive(Names.DatePicker.dateRangeDirective, function () {
        var directive = {
            require: Names.Validators.ngModelController,
            restrict: "A",
            link: function ($scope, $element, attrs, ngModelController) {
                new DateRangeValidator($scope, $element, attrs, ngModelController).link();
            }
        };
        return directive;
    });

    //override bootstrap's templates
    angular.module("template/datepicker/popup.html", []).run(["$templateCache", function ($templateCache) {
            $templateCache.put("template/datepicker/popup.html", require("text!./Views/datePicker.html"));
        }]);

    angular.module("template/datepicker/day.html", []).run(["$templateCache", function ($templateCache) {
            $templateCache.put("template/datepicker/day.html", require("text!./Views/calendarDayPicker.html"));
        }]);
    return datePicker;
});