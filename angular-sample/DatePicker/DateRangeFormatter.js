define(["require", "exports", "./DateRangeParseUtils", "moment"],
    function (require, exports, DateRangeParseUtils, moment) {

    var DateRangeFormatter = (function () {

        function DateRangeFormatter(referenceDate) {
            this.referenceDate = referenceDate;
            this.singleDateFormat = "ddd, D MMM";
            this.startFormat = "D";
            this.stopFormat = "D MMM";
            this.differentMonthStartFormat = "D MMM";
            this.differentYearAddonFormat = "YYYY";
        }

        DateRangeFormatter.prototype.format = function (dateRange) {
            var today = this.referenceDate || moment.utc();
            var formatted = null;
            if (dateRange) {
                if (!dateRange.isRange()) {
                    var format = this.singleDateFormat;
                    if (!dateRange.start.isSame(today, "year")) {
                        format += " " + this.differentYearAddonFormat;
                    }
                    formatted = dateRange.start.format(format);
                }
                else {
                    var startFormat = this.startFormat;
                    var stopFormat = this.stopFormat;
                    var lastDate = dateRange.stop.clone().add(-1, "days").startOf("day");
                    if (!dateRange.start.isSame(lastDate, "month")) {
                        startFormat = this.differentMonthStartFormat;
                    }
                    if (!dateRange.start.isSame(lastDate, "year")) {
                        startFormat += " " + this.differentYearAddonFormat;
                        stopFormat += " " + this.differentYearAddonFormat;
                    }
                    else if (!lastDate.isSame(today, "year")) {
                        stopFormat += " " + this.differentYearAddonFormat;
                    }
                    formatted = dateRange.start.format(startFormat)
                        + " - "
                        + lastDate.format(stopFormat);
                }
            }
            return formatted;
        };

        DateRangeFormatter.prototype.parse = function (value, today) {
            var dateRange;
            var dateStrings = value.split("-").map(function (dateString) { return dateString.trim(); });
            var matches = DateRangeParseUtils.getRegexMatches(value);
            if (dateStrings.length !== matches.length) {
                matches = null;
            }
            var dateCount = dateStrings.length;
            var parsedDates = [];
            for (var i = 0; i < dateCount; i++) {
                var combinedDate = DateRangeParseUtils.parseAndCombine(matches ? matches[i] : null, dateStrings[i]);
                if (combinedDate) {
                    parsedDates.push(combinedDate);
                }
            }
            if (parsedDates.length === 1 && dateCount === 1) {
                dateRange = DateRangeParseUtils.convertSingleDate(parsedDates[0], today);
            }
            else if (parsedDates.length === 2 && dateCount === 2) {
                dateRange = DateRangeParseUtils.convertDateRange(parsedDates[0], parsedDates[1], today);
            }
            return dateRange;
        };
        return DateRangeFormatter;
    })();
    return DateRangeFormatter;
});