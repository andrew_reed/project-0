define(["require", "exports", "App/Names"], function (require, exports, Names) {
    var DropdownController = (function () {
        function DropdownController($scope) {
            this.$scope = $scope;
            this.items = [];
            this.disabled = false;
        }
        DropdownController.$inject = [Names.$scope];
        return DropdownController;
    })();
    return DropdownController;
});