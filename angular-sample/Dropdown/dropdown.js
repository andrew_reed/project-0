/// <amd-dependency path="ui-select" />
/// <amd-dependency path="text!./Views/dropdown.html" />
/// <amd-dependency path="text!./Views/dropdowns.html" />
/// <amd-dependency path="text!./Views/dropdownsModalLayout.html" />

define(["require", "exports", "angular", "App/Names", "./DropdownsModalLayoutController", "./ContractTypeDropdownController", "./CompanyCodeTypeDropdownController", "./ReportingApproachDropdownController", "./TradingCapacityDropdownController", "./BuySellIndicatorDropdownController", "ui-select", "text!./Views/dropdown.html", "text!./Views/dropdowns.html", "text!./Views/dropdownsModalLayout.html"],
    function (require, exports, angular, Names, DropdownsModalLayoutController, ContractTypeDropdownController, CompanyCodeTypeDropdownController, ReportingApproachDropdownController, TradingCapacityDropdownController, BuySellIndicatorDropdownController) {

    var dropdown = angular.module(Names.Dropdown.module, [Names.UI.Select.module, Names.ngSanitize]);
    dropdown.controller(Names.Dropdown.dropdownsModalLayoutController, DropdownsModalLayoutController);
    dropdown.controller(Names.Dropdown.reportingApproachDropdownController, ReportingApproachDropdownController);
    dropdown.controller(Names.Dropdown.contractTypeDropdownController, ContractTypeDropdownController);
    dropdown.controller(Names.Dropdown.companyCodeTypeDropdownController, CompanyCodeTypeDropdownController);
    dropdown.controller(Names.Dropdown.tradingCapacityDropdownController, TradingCapacityDropdownController);
    dropdown.controller(Names.Dropdown.buySellIndicatorDropdownController, BuySellIndicatorDropdownController);

    dropdown.config([Names.UI.Select.config, function (config) {
            config.theme = Names.UI.Select.bootstrapTheme;
            config.appendToBody = true;
        }]);

    var dropdownDirective = {
        restrict: "E",
        replace: true,
        template: require("text!./Views/dropdown.html"),
        controllerAs: "controller",
        scope: {
            selectedId: "=ngModel",
            label: "=?"
        }
    };

    dropdown.directive(Names.Dropdown.reportingApproachDropdownDirective, function () {
        var directive = angular.extend({}, dropdownDirective, {
            controller: Names.Dropdown.reportingApproachDropdownController
        });
        return directive;
    });

    dropdown.directive(Names.Dropdown.contractTypeDropdownDirective, function () {
        var directive = angular.extend({}, dropdownDirective, {
            controller: Names.Dropdown.contractTypeDropdownController
        });
        return directive;
    });

    dropdown.directive(Names.Dropdown.companyCodeTypeDropdownDirective, function () {
        var directive = angular.extend({}, dropdownDirective, {
            controller: Names.Dropdown.companyCodeTypeDropdownController,
            scope: {
                codeTypeSource: "=",
                selectedItemChanged: "&",
                label: "=?"
            }
        });
        return directive;
    });

    dropdown.directive(Names.Dropdown.tradingCapacityDropdownDirective, function () {
        var directive = angular.extend({}, dropdownDirective, {
            controller: Names.Dropdown.tradingCapacityDropdownController
        });
        return directive;
    });

    dropdown.directive(Names.Dropdown.buySellIndicatorDropdownDirective, function () {
        var directive = angular.extend({}, dropdownDirective, {
            controller: Names.Dropdown.buySellIndicatorDropdownController
        });
        return directive;
    });

    dropdown.directive(Names.Dropdown.dropdownsDirective, function () {
        var directive = {
            restrict: "E",
            replace: true,
            controller: Names.Dropdown.dropdownsController,
            controllerAs: "controller",
            template: require("text!./Views/dropdowns.html"),
            scope: {
                selectedReportingApproachId: "=?",
                selectedItemsChanged: "&"
            }
        };
        return directive;
    });

    dropdown.directive(Names.Dropdown.dropdownsModalLayoutDirective, function () {
        var directive = {
            restrict: "E",
            replace: true,
            transclude: true,
            controller: Names.Dropdown.dropdownsModalLayoutController,
            controllerAs: "controller",
            template: require("text!./Views/dropdownsModalLayout.html"),
            scope: {
                titleText: "@",
                modalOpened: "=",
                isDirty: "=?",
                isSaving: "=?",
                save: "&",
                cancel: "&",
                messagesScope: "="
            }
        };
        return directive;
    });
    return dropdown;
});