define(["require", "exports", "App/Names"], function (require, exports, Names) {
    var DropdownsModalLayoutController = (function () {
        function DropdownsModalLayoutController($scope) {
            this.$scope = $scope;
        }

        Object.defineProperty(DropdownsModalLayoutController.prototype, "isProcessing", {
            get: function () {
                return this.$scope.isSaving;
            },
            enumerable: true,
            configurable: true
        });

        Object.defineProperty(DropdownsModalLayoutController.prototype, "titleText", {
            get: function () {
                return this.$scope.titleText;
            },
            enumerable: true,
            configurable: true
        });

        DropdownsModalLayoutController.prototype.getProcessingMessage = function () {
            return null;
        };

        DropdownsModalLayoutController.prototype.saveClicked = function ($event) {
            this.$scope.save();
        };

        DropdownsModalLayoutController.prototype.cancelClicked = function ($event) {
            this.$scope.cancel();
        };

        DropdownsModalLayoutController.$inject = [Names.$scope];
        return DropdownsModalLayoutController;
    })();
    return DropdownsModalLayoutController;
});