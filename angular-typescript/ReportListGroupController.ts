﻿import Names = require("App/Names");
import Events = require("App/Events");
import Event = require("App/Common/Events/Event");

class ReportListGroupController implements JNC.Report.IReportListGroupController {

    totalListsExpected: number = 2;

    nsContractList: JNC.Report.IReportListController;
    executions: JNC.Report.IReportListController;

    visibleReportList: JNC.Report.IReportListController;
    reportCount: JNC.Report.IReportCount;

    private totalReportCountChangedEvent: Event<JNC.Report.IReportCount>;
    private deregisterListInitialsedWatch: Function;
    
    static $inject = [
        Names.$scope,
        Names.$rootScope,
        Names.Application.localStorageService,
        Names.Application.userService,
    ];

    constructor($scope: ng.IScope,
        $rootScope: ng.IScope,
        private localStorageService: JNC.ILocalStorageService,
        private userService: JNC.Authorization.IUserService) {

        this.visibleReportList = this.nsContractList;

        this.totalReportCountChangedEvent = new Event<JNC.Report.IReportCount>(Events.Report.totalReportCountChanged, $rootScope);

        this.deregisterListInitialsedWatch = $scope.$watch(() => this.allReportListsInitialised,
            this.reportListsInitializedChanged.bind(this));

        $scope.$watch(() => this.nsContractList && this.nsContractList.reportCount, this.reportCountChanged.bind(this), true);
        $scope.$watch(() => this.executions && this.executions.reportCount, this.reportCountChanged.bind(this), true);
    };

    getLocalStorageKey(): string {
        return this.userService.user.username + "SelectedReportListTab";
    }

    private get allReportListsInitialised(): boolean {
        return this.reportLists.length === this.totalListsExpected;
    }

    private reportCountChanged(): void {
        var sum: JNC.Report.IReportCount;
        var reportCounts: JNC.Report.IReportCount[] = this.reportLists.map(
            (reportList: JNC.Report.IReportListController) => reportList.reportCount);

        sum = reportCounts.reduce((sum: JNC.Report.IReportCount, current: JNC.Report.IReportCount) => {
            if (current) {
                sum.failed += current.failed;
                sum.total += current.total;
                sum.visibleFailed += current.visibleFailed;
                sum.visibleTotal += current.visibleTotal;
            }
            return sum;
        }, this.reportCountSumSeed());
        
        this.reportCount = sum;
        this.totalReportCountChangedEvent.broadcast(this.reportCount);
    }

    private reportCountSumSeed(): JNC.Report.IReportCount {
        var reportCountsLoaded: boolean = this.allReportListsInitialised
            && this.reportLists.every((reportList: JNC.Report.IReportListController) => {
                return reportList.reportCount && !isNaN(reportList.reportCount.total);
            });
        return {
            failed: 0,
            total: reportCountsLoaded ? 0 : NaN,
            visibleFailed: 0,
            visibleTotal: 0
        };
    }

    private reportListsInitializedChanged(): void {
        if (this.allReportListsInitialised) {
            var stored: JNC.Report.IReportListController = this.getStoredSelectedReportList();
            if (stored) {
                this.setVisibleReportList(stored);
            } else {
                this.setVisibleReportList(this.nsContractList);
            }
            this.deregisterListInitialsedWatch();
        }
    }

    get reportLists(): JNC.Report.IReportListController[] {
        var reportLists: JNC.Report.IReportListController[] = [];

        if (this.nsContractList) {
            reportLists.push(this.nsContractList);
        }
        if (this.executions) {
            reportLists.push(this.executions);
        }
        return reportLists;
    }

    getStoredSelectedReportList(): JNC.Report.IReportListController {
        var storedName = this.localStorageService.get<string>(this.getLocalStorageKey());
        return _.find(this.reportLists, (reportList: JNC.Report.IReportListController) => {
            return reportList.getTypeName() === storedName;
        });
    }

    setVisibleReportList(reportList: JNC.Report.IReportListController): void {
        this.localStorageService.set(this.getLocalStorageKey(), reportList.getTypeName());
        this.visibleReportList = reportList;
    }

    isReportListVisible(reportList: JNC.Report.IReportListController): boolean {
        return this.visibleReportList === reportList;
    }
}

export = ReportListGroupController;