﻿module JNC.Report {
    export interface IReportListController extends IHasReportCount {
        getDisplayName(): string;
        getTypeName(): string;
    }
}