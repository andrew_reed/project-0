﻿/// <amd-dependency path="text!./Views/GridTemplates/defaultHeaderCell.html" />
/// <amd-dependency path="text!./Views/GridTemplates/dateCell.html" />
/// <amd-dependency path="text!./Views/GridTemplates/timeCell.html" />
/// <amd-dependency path="text!./Views/GridTemplates/actionsCell.html" />
/// <amd-dependency path="text!./Views/GridTemplates/memberCell.html" />
/// <amd-dependency path="text!./Views/GridTemplates/defaultCell.html" />
/// <amd-dependency path="text!./Views/GridTemplates/completedCell.html" />
/// <amd-dependency path="text!./Views/GridTemplates/lastUpdatedCell.html" />
/// <amd-dependency path="text!./Views/GridTemplates/reasonabilityCell.html" />
/// <amd-dependency path="text!./Views/GridTemplates/emptyHeaderCell.html" />
/// <amd-dependency path="text!./Views/GridTemplates/tradedNetVolumeCell.html" />
/// <amd-dependency path="text!./Views/GridTemplates/row.html" />
/// <amd-dependency path="text!./Views/GridTemplates/primaryCounterpartyCell.html" />

import Names = require("App/Names");
import MessageType = require("App/Messages/MessageType");
import SelectorType = require("App/Selector/SelectorType");
import Event = require("App/Common/Events/Event");
import Events = require("App/Events");

class ReportListController<
    TReport extends JNC.Report.IReport,
    TService extends JNC.Report.IReportService<JNC.Report.IReport>>
    implements JNC.Report.IReportListController {
    reports: TReport[];
    visibleReports: TReport[] = [];
    reportCount: JNC.Report.IReportCount;
    grid: ui.grid.IGrid;
    gridResizer: JNC.Grid.IGridResizerController;
    selectedFilters: { [index: number]: JNC.Selector.ISelectorItem[] } = {};
    private filtersChangedEvent: Event<JNC.Filter.IFiltersChangedEventArguments>;
    private filtersLayoutChangedEvent: Event<number>;
    raiseMessageEvent: Event<JNC.Messages.IRaisedMessage>;

    sortDirectionCycleDesc: string[] = [this.gridConstants.DESC, this.gridConstants.ASC];
    sortDirectionCycleAsc: string[] = [this.gridConstants.ASC, this.gridConstants.DESC];

    reasonableColumn: JNC.Report.IReportColumnDefinition = {
        name: "reasonable",
        field: "status",
        cellClass: "reasonable",
        headerCellClass: "reasonable",
        enableSorting: false,
        width: "20",
        cellTemplate: require("text!./Views/GridTemplates/reasonabilityCell.html"),
        headerCellTemplate: require("text!./Views/GridTemplates/emptyHeaderCell.html")
    };

    counterparty1Column: JNC.Report.IReportColumnDefinition = {
        name: "counterparty1",
        idField: "id",
        field: "counterparty1",
        minWidth: 100,
        sortDirectionCycle: this.sortDirectionCycleAsc,
        cellClass: "small",
        headerCellClass: "small",
        cellTemplate: require("text!./Views/GridTemplates/primaryCounterpartyCell.html"),
        headerCellTemplate: require("text!./Views/GridTemplates/defaultHeaderCell.html")
    };

    contractColumn: JNC.Report.IReportColumnDefinition = {
        name: "name",
        field: "name",
        idField: "id",
        filterType: SelectorType.ContractName,
        displayName: "Contract",
        minWidth: 100,
        sortDirectionCycle: this.sortDirectionCycleAsc,
        cellClass: "small",
        headerCellClass: "small",
        cellTemplate: require("text!./Views/GridTemplates/defaultCell.html"),
        headerCellTemplate: require("text!./Views/GridTemplates/defaultHeaderCell.html")
    };

    dateStartColumn: JNC.Report.IReportColumnDefinition = {
        name: "contractDate",
        width: 100,
        sortDirectionCycle: this.sortDirectionCycleDesc,
        cellTemplate: require("text!./Views/GridTemplates/dateCell.html"),
        cellClass: "center tiny",
        headerCellClass: "tiny",
        headerCellTemplate: require("text!./Views/GridTemplates/defaultHeaderCell.html")
    };

    lastUpdatedColumn: JNC.Report.IReportColumnDefinition = {
        name: "lastModified",
        field: "lastModified",
        displayName: "Last updated",
        width: 100,
        sort: {
            direction: this.gridConstants.DESC,
            priority: 0
        },
        sortingAlgorithm: (date1: any, date2: any) => {
            if (!(date1 && date2)) {
                return 0;
            }
            return date1.diff(date2);
        },
        sortDirectionCycle: this.sortDirectionCycleDesc,
        cellTemplate: require("text!./Views/GridTemplates/timeCell.html"),
        cellClass: "center tiny",
        headerCellClass: "tiny",
        headerCellTemplate: require("text!./Views/GridTemplates/defaultHeaderCell.html")
    };

    statusColumn: JNC.Report.IReportColumnDefinition = {
        name: "status",
        field: "status",
        displayName: "Status",
        filterType: SelectorType.Status,
        sortDirectionCycle: this.sortDirectionCycleAsc,
        sortingAlgorithm: (status1: any, status2: any) => {
            var order = {};
            order[JNC.Report.ReportStatus.Draft] = 0;
            order[JNC.Report.ReportStatus.DraftLastValidationRejected] = 1;
            order[JNC.Report.ReportStatus.DraftLastValidationPassed] = 2;
            order[JNC.Report.ReportStatus.BeingValidated] = 3;
            order[JNC.Report.ReportStatus.ValidationRejected] = 4;
            order[JNC.Report.ReportStatus.ValidationPassed] = 5;
            order[JNC.Report.ReportStatus.Terminated] = 6;
            order[JNC.Report.ReportStatus.Canceled] = 7;

            if (order[status1] > order[status2]) {
                return 1;
            }
            if (order[status1] < order[status2]) {
                return -1;
            }
            return 0;
        },
        maxWidth: 90,
        minWidth: 80,
        width: "*",
        cellClass: "center tiny",
        headerCellClass: "tiny",
        cellTemplate: require("text!./Views/GridTemplates/completedCell.html"),
        headerCellTemplate: require("text!./Views/GridTemplates/defaultHeaderCell.html")
    };

    actionsColumn: JNC.Report.IReportColumnDefinition = {
        name: "actions",
        displayName: "Actions",
        width: 50,
        enableSorting: false,
        cellClass: "tiny cell-visible",
        headerCellClass: "tiny",
        cellTemplate: require("text!./Views/GridTemplates/actionsCell.html"),
        headerCellTemplate: require("text!./Views/GridTemplates/defaultHeaderCell.html")
    };

    columns: JNC.Report.IReportColumnDefinition[] = [//todo 29 check columns width
        this.reasonableColumn,
        this.counterparty1Column,
        this.contractColumn,
        this.dateStartColumn,
        this.lastUpdatedColumn,
        this.statusColumn,
        this.actionsColumn
    ];

    gridOptions: ui.grid.IGridOptions = {
        data: "controller.visibleReports",
        rowTemplate: require("text!./Views/GridTemplates/row.html"),
        columnDefs: this.columns,
        columnVirtualizationThreshold: Infinity,
        virtualizationThreshold: 50,
        enableColumnMenus: false,
        onRegisterApi: (gridApi) => {
            this.grid = gridApi.grid;
        }
    };

    isLoading: boolean = false;

    longDateFormat: string = "YYYY-MM-DD";
    timeFormat: string = "HH:mm";
    shortDateFormat: string = "D MMM";
    dateFormat: string = this.longDateFormat;

    static $inject = [
        Names.$scope,
        Names.$parse,
        Names.$window,
        Names.$timeout,
        Names.UI.gridConstants,
    ];
    constructor(
        public $scope: JNC.Report.IReportListScope,
        public $parse: ng.IParseService,
        public $window: ng.IWindowService,
        public $timeout: ng.ITimeoutService,
        public gridConstants: ui.grid.IGridConstants,
        public reportService: TService,
        public apiService?: JNC.Report.IAPIService) {

        //Propagate this controller to user scope double-way bound variable
        $scope.controller = this;

        this.filtersLayoutChangedEvent = new Event(Events.Filter.filtersLayoutChanged, $scope);
        this.filtersLayoutChangedEvent.on(this.layoutChanged.bind(this));

        this.filtersChangedEvent = new Event(Events.Filter.filtersChanged, $scope);
        this.filtersChangedEvent.on(this.filtersChanged.bind(this));
        this.raiseMessageEvent = new Event<JNC.Messages.IRaisedMessage>(Events.Messages.raiseMessage, $scope);

        this.updateReports();
    }

    get isVisible(): boolean {
        return this.$scope.isVisible;
    }

    get showGrid(): boolean {
        return this.isVisible && this.visibleReports && this.visibleReports.length > 0;
    }

    get isLoaded(): boolean {
        return !this.isLoading;
    }

    notifyDataChange(dataChangeType: string) {
        if (this.grid) {
            this.grid.api.core.notifyDataChange(dataChangeType);
        }
    }

    layoutChanged(event: ng.IAngularEvent, contentWidth: number): void {
        if (this.gridResizer) {
            this.gridResizer.resize();
        }
        this.notifyDataChange(this.gridConstants.dataChange.COLUMN);
    }

    getDisplayName(): string {
        throw new Error("Not Implemented");
    }

    getTypeName(): string {
        throw new Error("Not Implemented");
    }

    updateReports() {
        this.isLoading = true;
        this.createReportCount(null);
        this.getReports()
            .then((result: ng.IHttpPromiseCallbackArg<TReport[]>) => {
                this.getReportsSuccess(result.data);
            })
            .finally(() => this.getReportsFinally());
    }

    getReports(): JNC.IHttpPromise<TReport[]> {
        var promise: JNC.IHttpPromise<TReport[]> = this.reportService.getReports();
        promise.catch((reason: any) => {
            if (!promise.canceled) {
                this.raiseMessageEvent.emit({
                    text: "Error fetching {reportListDisplayName}",
                    parameters: {
                        reportListDisplayName: this.getDisplayName()
                    },
                    type: MessageType.Error
                });
                throw reason;
            }
        });
        return promise;
    }

    getReportsSuccess(reports: TReport[]) {
        this.reports = reports;
        this.updateVisibleReports();
    }

    getReportsFinally() {
        this.createReportCount(this.reports);
        this.isLoading = false;
    }

    filtersChanged(event: ng.IAngularEvent, args: JNC.Filter.IFiltersChangedEventArguments) {
        this.selectedFilters = args.selectedFilters || {};
        this.toggleColumnVisibility();
        this.updateVisibleReports();
        this.createReportCount(this.reports);
    }

    updateVisibleReports() {
        if (this.reports) {
            // For each of the report, filter report
            var reports: TReport[] = this.reports.filter((report: TReport) => {
                // Report is filter only when all columns are filtered
                return this.columns.every((column) => this.filterByColumn(report, column));
            });
            this.visibleReports = reports;
        }
    }

    filterByColumn(report: TReport, column: JNC.Report.IReportColumnDefinition): boolean {
        var match: boolean = true;
        if (column.filterType
            && this.selectedFilters[column.filterType]
            && this.selectedFilters[column.filterType].length > 0
            && this.selectedFilters[column.filterType][0].id !== null) {
            var field: string = column.idField || column.field;
            var idFieldGetter = this.$parse(field);
            var filters: JNC.Selector.ISelectorItem[] = this.selectedFilters[column.filterType];
            match = filters.some((filter: JNC.Selector.ISelectorItem) => {
                if ((<JNC.Selector.IStatusSelectorItem>filter).matcher) {
                    return (<JNC.Selector.IStatusSelectorItem>filter).matcher(report);
                }
                return idFieldGetter(report) === filter.id;
            });
        }
        return match;
    }

    createReportCount(reports: TReport[]): void {
        var reportCount: JNC.Report.IReportCount = {
            total: NaN,
            failed: 0,
            visibleFailed: 0,
            visibleTotal: 0
        };
        if (reports) {
            reportCount.total = reports.length;
            reportCount.failed = this.countFailedReports(reports);
            reportCount.visibleTotal = this.visibleReports.length;
            reportCount.visibleFailed = this.countFailedReports(this.visibleReports);
        }
        this.reportCount = reportCount;
    }

    isReportFailed(report: TReport): boolean {
        return report.status === JNC.Report.ReportStatus.ValidationRejected ||
            report.status === JNC.Report.ReportStatus.DraftLastValidationRejected;
    }

    countFailedReports(reports: TReport[]): number {
        return reports.filter(this.isReportFailed, this).length;
    }

    reasonabilityCheckResultClicked(
        event: JQueryEventObject,
        gridRow: JNC.Report.IReportGridRow<any>,
        popout: JNC.Popout.IPopout<JNC.Report.IReasonabilityCheckResultPopoutContext>) {
        event.preventDefault();
        this.toggleReasonabilityCheckResultPopout(popout, gridRow);
    }

    toggleReasonabilityCheckResultPopout(
        reasonabilityCheckResultPopout: JNC.Popout.IPopout<JNC.Report.IReasonabilityCheckResultPopoutContext>,
        gridRow: JNC.Report.IReportGridRow<any>) {
        var context: JNC.Report.IReasonabilityCheckContext;
        var entity: any = gridRow.entity;
        context = this.getReportReasonabilityCheckContext(entity);
        reasonabilityCheckResultPopout.toggle(context);
    }

    getReportName(report: TReport): string {
        throw new Error("Not implemented");
    }

    getReportReasonabilityCheckContext(report: TReport): JNC.Report.IReasonabilityCheckContext {
        var result: JNC.Report.IReasonabilityCheckResult = this.getReasonabilityCheckResult.bind(this)(report);
        return {
            reportErrorResults: [result]
        };
    }

    getReasonabilityCheckResult(report: TReport): JNC.Report.IReasonabilityCheckResult {
        //TODO implement check
        return;
    }

    toggleColumnVisibility() {
        var isSingleCounterpartySelected = this.selectedFilters[SelectorType.PrimaryCounterparty]
                                            && this.selectedFilters[SelectorType.PrimaryCounterparty].length === 1;
        if (isSingleCounterpartySelected) {
            this.counterparty1Column.visible = false;
        } else {
            this.counterparty1Column.visible = true;
        }
    }

    submitAcer(entity: JNC.Report.IReport): boolean {
        throw new Error("Not implemented");
    }

    sendSubmitAcer(id: string) {
        var promise: JNC.IHttpPromise<any> = this.apiService.postAction("submit", id);
        promise.catch((reason: any) => {
            if (!promise.canceled) {
                this.raiseMessageEvent.emit({
                    text: "Error submitting to ACER",
                    parameters: {},
                    type: MessageType.Error
                });
                throw reason;
            }
        });
        return promise;
    }

    sendDelete(id: string) {
        var promise: JNC.IHttpPromise<any> = this.apiService.postAction("delete", id);
        promise.catch((reason: any) => {
            if (!promise.canceled) {
                this.raiseMessageEvent.emit({
                    text: "Error deleting",
                    parameters: {},
                    type: MessageType.Error
                });
                throw reason;
            }
        });
        return promise;
    }
}

export = ReportListController;