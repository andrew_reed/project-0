﻿import ServiceBase = require("App/Common/Services/ServiceBase");

class ReportService<TReport extends NPS.Report.IReport>
    extends ServiceBase implements NPS.Report.IReportService<TReport> {
    static $inject: string[] = ServiceBase.$inject;

    constructor(
        $http: ng.IHttpService,
        $q: ng.IQService,
        $cacheFactory: ng.ICacheFactoryService,
        $window: ng.IWindowService,
        $rootScope: ng.IRootScopeService,
        serviceUrl: string) {
        super(
            $http,
            $q,
            $cacheFactory,
            $window,
            $rootScope,
            serviceUrl);
    }

    getReports(): NPS.IHttpPromise<TReport[]> {
        var promise: NPS.IHttpPromise<TReport[]> = super.get("");
        promise.success((json: TReport[]) => {
            json.forEach((json: TReport) => {
                json.lastModified = moment(json.lastModified);
                json.contractDate = moment(json.contractDate);
            });
        });
        return promise;
    }
}

export = ReportService;