﻿module NPS.Report {
    export interface IReportService<TReport extends NPS.Report.IReport> {
        getReports(): NPS.IHttpPromise<TReport[]>;
    }
}