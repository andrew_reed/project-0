﻿module JNC.Report {
    export interface IReportListGroupController extends IHasReportCount {
        setVisibleReportList(reportList: JNC.Report.IHasReportCount): void;
        isReportListVisible(reportList: JNC.Report.IHasReportCount): boolean;
    }
} 
