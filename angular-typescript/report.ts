﻿/// <amd-dependency path="text!./Views/reportList.html" />
/// <amd-dependency path="text!./Views/reportListSummary.html" />
/// <amd-dependency path="text!./Views/reportListGroup.html" />
/// <amd-dependency path="text!./Views/reportListSelector.html" />
/// <amd-dependency path="text!./Views/status.html" />
/// <amd-dependency path="text!./Views/lastUpdated.html" />
/// <amd-dependency path="text!./Views/reportFile.html" />
/// <amd-dependency path="text!./Views/reportFileDownload.html" />
/// <amd-dependency path="text!./Views/actionsMenu.html" />

//TODO rename to Contract

import angular = require("angular");
import Names = require("App/Names");
import reportReasonabilityCheck = require("./ReportReasonabilityCheck/reportReasonabilityCheck");
import ReportListController = require("./ReportListController");
import ReportListGroupController = require("./ReportListGroupController");
import ReportListSelectorController = require("./ReportListSelectorController");
import ReportListSummaryController = require("./ReportListSummaryController");
import StatusController = require("./StatusController");
import ActionController = require("./ActionController");
import ReportFileController = require("./ReportFileController");
import ReportFileDownloadController = require("./ReportFileDownloadController");
import LastUpdatedController = require("./LastUpdatedController");
import ReportService = require("./ReportService");
import APIService = require("./APIService");

var report: ng.IModule = angular.module(Names.Report.module, [
    reportReasonabilityCheck.name
]);

report.controller(Names.Report.reportListController, ReportListController);
report.controller(Names.Report.reportListSummaryController, ReportListSummaryController);
report.controller(Names.Report.reportListGroupController, ReportListGroupController);
report.controller(Names.Report.reportListSelectorController, ReportListSelectorController);
report.controller(Names.Report.statusController, StatusController);
report.controller(Names.Report.actionController, ActionController);
report.controller(Names.Report.reportFileController, ReportFileController);
report.controller(Names.Report.reportFileDownloadController, ReportFileDownloadController);
report.controller(Names.Report.lastUpdatedController, LastUpdatedController);

report.service(Names.Report.reportService, ReportService);
report.service(Names.Report.apiService, APIService);

report.directive(Names.Report.reportListSummaryDirective, () => {
    var directive: ng.IDirective = {
        restrict: "E",
        replace: true,
        controller: Names.Report.reportListSummaryController,
        controllerAs: "controller",
        require: "ngModel",
        scope: {
            tag: "@",
            reportCount: "=",
            showCountMessage: "=?",
            showRejected: "=?",
            showVisible: "=?",
            showReasonabilityFlag: "=",
            clicked: "&?",
            isSelector: "=?",
            selected: "="
        },
        template: require("text!./Views/reportListSummary.html")
    };
    return directive;
});

report.directive(Names.Report.reportListGroupDirective, function () {
    var directive: ng.IDirective = {
        restrict: "E",
        replace: true,
        controller: Names.Report.reportListGroupController,
        controllerAs: "controller",
        template: require("text!./Views/reportListGroup.html"),
        scope:
        {
        }
    };
    return directive;
});

report.directive(Names.Report.reportListSelectorDirective, function () {
    var directive: ng.IDirective = {
        restrict: "E",
        replace: true,
        controller: Names.Report.reportListSelectorController,
        controllerAs: "controller",
        template: require("text!./Views/reportListSelector.html"),
        scope:
        {
            reportLists: "=*",
            selectedReportList: "=",
            selectReportList: "&"
        }
    };
    return directive;
});

report.directive(Names.Report.statusDirective, () => {
    var directive: ng.IDirective = {
        restrict: "E",
        replace: true,
        controller: Names.Report.statusController,
        controllerAs: "controller",
        scope: {
            status: "="
        },
        template: require("text!./Views/status.html")
    };
    return directive;
});

report.directive(Names.Report.actionDirective, () => {
    var directive: ng.IDirective = {
        restrict: "E",
        replace: true,
        controller: Names.Report.actionController,
        controllerAs: "controller",
        scope: {
            view: "&",
            edit: "&",
            submit: "&",
            delete: "&",
            entity: "=",
            title: "@"
        },
        template: require("text!./Views/actionsMenu.html")
    };
    return directive;
});

report.directive(Names.Report.lastUpdatedDirective, () => {
    var directive: ng.IDirective = {
        restrict: "E",
        replace: true,
        controller: Names.Report.lastUpdatedController,
        controllerAs: "controller",
        scope: {
            lastUpdated: "="
        },
        template: require("text!./Views/lastUpdated.html")
    };
    return directive;
});

report.directive(Names.Report.reportFileDirective, () => {
    var directive: ng.IDirective = {
        restrict: "E",
        replace: true,
        controller: Names.Report.reportFileController,
        controllerAs: "controller",
        scope: {
            reportFile: "="
        },
        template: require("text!./Views/reportFile.html")
    };
    return directive;
});


report.directive(Names.Report.reportFileDownloadDirective, () => {
    var directive: ng.IDirective = {
        restrict: "E",
        replace: true,
        controller: Names.Report.reportFileDownloadController,
        controllerAs: "controller",
        scope: {
            uri: "="
        },
        template: require("text!./Views/reportFileDownload.html")
    };
    return directive;
});

export = report;
